import os
from transformers import AutoModelForCausalLM, AutoTokenizer
from transformers import GPTJForCausalLM
from transformers import set_seed
import torch


# https://huggingface.co/blog/how-to-generate


def play(torch_device, tokenizer, model, prompt, seed=42):
    # encode context the generation is conditioned on
    model_inputs = tokenizer(prompt, return_tensors='pt').to(torch_device)

    # generate greedily
    set_seed(seed)
    greedy_output = model.generate(**model_inputs, max_new_tokens=40)
    print("\nGreedy output:\n" + 100 * '-')
    print(tokenizer.decode(greedy_output[0], skip_special_tokens=True))

    # generate using beam search
    set_seed(seed)
    beam_outputs = model.generate(
        **model_inputs,
        max_new_tokens=40,
        num_beams=5,
        early_stopping=True,
        num_return_sequences=3,
    )
    print("\nBeam output:\n" + 100 * '-')
    for bx, beam_output in enumerate(beam_outputs):
        print(f"{bx}: {tokenizer.decode(beam_output, skip_special_tokens=True)}")

    # Change temperature and sample
    set_seed(seed)
    sample_output = model.generate(
        **model_inputs,
        max_new_tokens=40,
        do_sample=True,
        top_k=0,
        temperature=0.6,
    )
    print("\nSample output:\n" + 100 * '-')
    print(tokenizer.decode(sample_output[0], skip_special_tokens=True))

    # Top-k sample
    set_seed(seed)
    topk_sample_output = model.generate(
        **model_inputs,
        max_new_tokens=40,
        do_sample=True,
        top_k=50,
        temperature=0.6,
    )
    print("\nTop-k sample output:\n" + 100 * '-')
    print(tokenizer.decode(topk_sample_output[0], skip_special_tokens=True))

    # Nucleus sample
    set_seed(seed)
    nucleus_output = model.generate(
        **model_inputs,
        max_new_tokens=40,
        do_sample=True,
        top_p=0.92,
        top_k=0,
        temperature=0.6,
    )
    print("\nNucleus sample output:\n" + 100 * '-')
    print(tokenizer.decode(nucleus_output[0], skip_special_tokens=True))
    return


if __name__ == "__main__":
    assert os.getenv("HF_DATASETS_CACHE") is not None, \
        "set huggingface cache path env variable HF_DATASETS_CACHE"
    cache_dir = os.getenv("HF_DATASETS_CACHE")
    torch_device = "cuda" if torch.cuda.is_available() else "cpu"
    print(f"torch_device={torch_device}")

    gpt2_tokenizer = AutoTokenizer.from_pretrained("gpt2", cache_dir=cache_dir)
    gpt2_model = AutoModelForCausalLM.from_pretrained("gpt2",
        pad_token_id = gpt2_tokenizer.eos_token_id,
        cache_dir=cache_dir).to(torch_device)
    print("\n\n" + 20*"#" + " gpt2 " + 20*"#" + "\n")
    play(torch_device=torch_device, tokenizer=gpt2_tokenizer,
         model=gpt2_model, prompt= 'I enjoy walking with my cute dog')
    gpt2_tokenizer = None
    gpt2_model = None
         
    gptj_tokenizer = AutoTokenizer.from_pretrained("EleutherAI/gpt-j-6B",
                                                   cache_dir=cache_dir)
    gptj_model = GPTJForCausalLM.from_pretrained("EleutherAI/gpt-j-6B",
        revision="float32", cache_dir=cache_dir).to(torch_device)
    print("\n\n" + 20*"#" + " gptj " + 20*"#" + "\n")
    play(torch_device=torch_device, tokenizer=gptj_tokenizer,
         model=gptj_model, prompt= 'I enjoy walking with my cute dog')
    pass
