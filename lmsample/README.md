Demonstrates difference in behavior of language models when the output token sampling method is varied.  Needs 26GB RAM either with GPU or CPU.

Steps to prepare a conda environment:

1. conda create -p /path/to/env python=3.9
1. conda activate /path/to/env
1. conda install pytorch=2.2.0 torchvision torchaudio pytorch-cuda=11.8 -c pytorch -c nvidia
1. conda install transformers=4.32.1
